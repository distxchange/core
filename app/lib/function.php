<?php
function get($name,$def=''){

    return isset($_REQUEST[$name]) ? $_REQUEST[$name] : $def;


}

function cdn($hash){
  if($hash == "dtx"){
    $type = "image/png";
  }else{
    $type = "text/css";
  }


  return explode("/",$type);
}

function mime_type($ext){
  switch($ext){
    case 'png':
      return "image/png";
      break;
    case 'jpg':
    case 'jpeg':
      return "image/jpeg";
      break;
    case 'gif':
      return "image/gif";
      break;
    case 'ico':
      return "image/x-icon";
      break;
    case 'xml':
      return "application/xml";
      break;
    case 'json':
      return "application/json";
      break;
    case 'css':
      return "text/css";
      break;
    case 'html':
      return "text/html";
      break;
    case 'js':
      return "text/javascript";
      break;
    case 'mp4':
      return "video/mp4";
      break;
    case 'wav':
      return "audio/x-wav";
      break;
    case 'mp3':
    case 'mp2':
      return "audio/x-mpeg";
      break;
    case 'zip':
      return "application/zip";
      break;
    case 'tar':
      return "application/x-tar";
      break;
    case 'php':
      return "application/x-httpd-php";
      break;
    case 'pdf':
      return "application/pdf";
      break;
    case 'txt':
      return "text/plain";
      break;
    case 'mpeg':
    case 'mpe':
    case 'mpg':
      return "video/mpeg";
      break;
  }
}
?>
